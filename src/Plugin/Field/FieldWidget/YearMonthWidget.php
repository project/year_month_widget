<?php

declare(strict_types=1);

namespace Drupal\year_month_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeWidgetBase;

/**
 * Plugin implementation of the 'year_month_widget' widget.
 */
#[FieldWidget(
  id: 'year_month_widget',
  label: new TranslatableMarkup('Year/month'),
  field_types: ['datetime'],
  weight: 20,
)]
class YearMonthWidget extends DateTimeWidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'part_order' => 'YD',
      'year_range' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#theme_wrappers'][] = 'fieldset';
    $part_order = match ($this->getSetting('part_order')) {
      'MY' => ['month', 'year'],
      default => ['year', 'month'],
    };
    $datelist = [
      '#type' => 'datelist',
      '#date_part_order' => $part_order,
    ];
    if ($year_range = $this->getSetting('year_range')) {
      $datelist['#date_year_range'] = $year_range;
    }
    $element['value'] = $datelist + $element['value'];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element = parent::settingsForm($form, $form_state);
    $element['part_order'] = [
      '#type' => 'select',
      '#title' => $this->t('Part order'),
      '#options' => [
        'MY' => $this->t('Month/Year'),
        'YM' => $this->t('Year/Month'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->getSetting('part_order'),
    ];
    $element['year_range'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Year range'),
      '#description' => $this->t('Examples: -3:+1, 2000:2010, 2000:+3'),
      '#default_value' => $this->getSetting('year_range'),
      '#placeholder' => '1900:2050',
      '#size' => 12,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary[] = $this->t('Part order: @order', ['@order' => $this->getSetting('part_order')]);
    $summary[] = $this->t('Year range: @range', ['@range' => $this->getSetting('year_range') ?: $this->t('Default')]);
    return $summary;
  }

}
