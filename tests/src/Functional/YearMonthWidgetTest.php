<?php

declare(strict_types=1);

namespace Drupal\Tests\year_month_widget\Functional;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\datetime\Functional\DateTestBase;

/**
 * @coversDefaultClass \Drupal\year_month_widget\Plugin\Field\FieldWidget\YearMonthWidget
 */
class YearMonthWidgetTest extends DateTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['year_month_widget'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function getTestFieldType(): string {
    return 'datetime';
  }

  /**
   * Test the widget.
   */
  public function testWidget(): void {
    // Create a testing configuration.
    $bundle = 'content_test_datetime';
    $this->drupalCreateContentType(['type' => $bundle]);

    $field_name = 'field_test_datetime';
    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => 'datetime',
      'settings' => ['datetime_type' => 'date'],
    ]);
    $field_storage->save();
    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => $bundle,
      'required' => TRUE,
    ])->save();

    // Test the widget configuration.
    $widget_id = 'year_month_widget';
    $edit = [
      "fields[$field_name][region]" => 'content',
      "fields[$field_name][type]" => $widget_id,
    ];
    $this->drupalGet("admin/structure/types/manage/$bundle/form-display");
    $this->submitForm($edit, 'Save');

    $this->submitForm([], "{$field_name}_settings_edit");
    $edit = [
      "fields[$field_name][settings_edit_form][settings][part_order]" => 'MY',
      "fields[$field_name][settings_edit_form][settings][year_range]" => '2010:+10',
    ];
    $this->submitForm($edit, 'Update');
    $this->submitForm([], 'Save');

    // Test the widget's filling.
    $this->drupalGet("node/add/$bundle");
    $now = new DrupalDateTime('2017-12-01');
    $node_title = "$widget_id test";
    $edit = [
      'title[0][value]' => $node_title,
      "{$field_name}[0][value][year]" => $now->format('Y'),
      "{$field_name}[0][value][month]" => $now->format('m'),
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains("$bundle $node_title has been created");

    $node = $this->drupalGetNodeByTitle($node_title);
    $this->assertEquals($node->get($field_name)->getString(), $now->format('Y-m-01'));
  }

}
