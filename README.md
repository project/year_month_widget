# Year month widget

This module provides a plugin widget for fields of type "datetime"
to select only the month and year, skipping the day.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

## Configuration

1. After installation, go to the settings for displaying the form for
the desired entity.
2. In the list of widgets for the field, select the widget "Year/month"
3. Saving the form display for the entity.
